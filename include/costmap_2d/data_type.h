#ifndef COSTMAP_2D_DATA_TYPE_H_
#define COSTMAP_2D_DATA_TYPE_H_

#include <cmath>
#include <iostream>

namespace costmap_2d
{

    class Point
    {
    private:
        float m_x;
        float m_y;

    public:
        Point() : m_x(0),
                  m_y(0)
                  {}
        constexpr Point(float x, float y) : m_x(x),
                                           m_y(y){};


    public:
        friend bool operator==(const Point &l, const Point &r)
        {
            return l.x() == r.x() &&
                   l.y() == r.y();
        }
        friend bool operator!=(const Point &l, const Point &r)
        {
            return !(l == r);
        }

        friend Point operator+(const Point &l, const Point &r)
        {
            return Point(l.x() + r.x(), l.y() + r.y());
        }

        friend Point operator-(const Point &l, const Point &r)
        {
            return Point(l.x() - r.x(), l.y() - r.y());
        }

    public:
        float &x(void)
        {
            return m_x;
        };
        float x(void) const
        {
            return m_x;
        };

        float &y(void)
        {
            return m_y;
        };
        float y(void) const
        {
            return m_y;
        };


    public:
        std::string ToString()
        {
            std::string ss;
            ss = std::to_string(m_x) + "," + std::to_string(m_y) ;
            return ss;
        }

        template <class Archive>
        void serialize(Archive &ar, const unsigned int version)
        {
            ar & m_x;
            ar & m_y;
        }
    };
}
#endif
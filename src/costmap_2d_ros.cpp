/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2008, 2013, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Eitan Marder-Eppstein
 *         David V. Lu!!
 *********************************************************************/
#include <costmap_2d/layered_costmap.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <cstdio>
#include <string>
#include <algorithm>
#include <vector>
#include <tf2/convert.h>
#include <tf2/utils.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <costmap_2d/static_layer.h>
#include <costmap_2d/obstacle_layer.h>
#include <costmap_2d/inflation_layer.h>

#include <algorithm>

using namespace std;

namespace costmap_2d
{
    std::mutex mtx_pose;
    static inline Eigen::Isometry2d toIsometry2d(float x, float y, float theta)
    {
        Eigen::Isometry2d T;
        T.setIdentity();

        float c = cos(theta);
        float s = sin(theta);
        T.linear() << c, -s, s, c;
        T(0, 2) = x;
        T(1, 2) = y;
        return T;
    }

    Costmap2DROS::Costmap2DROS(const std::string &name, nlohmann::json &j_param, tf2_ros::Buffer &tf) : layered_costmap_(NULL),
                                                                                                        name_(name),
                                                                                                        tf_(tf),
                                                                                                        transform_tolerance_(0.3),
                                                                                                        map_update_thread_shutdown_(false),
                                                                                                        stop_updates_(false),
                                                                                                        initialized_(true),
                                                                                                        stopped_(false),
                                                                                                        robot_stopped_(false),
                                                                                                        map_update_thread_(NULL),
                                                                                                        move_check_thread_(NULL),
                                                                                                        // last_publish_(0),
                                                                                                        // plugin_loader_("costmap_2d", "costmap_2d::Layer"),
                                                                                                        publisher_(NULL),
                                                                                                        footprint_padding_(0.0)
    {

        old_pose_ = Eigen::Isometry2d::Identity();

        ros::NodeHandle private_nh("~/" + name);
        ros::NodeHandle g_nh;

        // FIXME: 去掉了tf获取判断等待

        bool rolling_window, track_unknown_space, always_send_full_costmap;

        auto custom_cost_param = j_param.at(name);
        global_frame_ = custom_cost_param.at("global_frame").get<std::string>();
        robot_base_frame_ = custom_cost_param.at("robot_base_frame").get<std::string>();
        rolling_window = custom_cost_param.at("rolling_window").get<bool>();

        // common param
        auto common_cost_param = j_param.at("costmap_common");
        track_unknown_space = common_cost_param.at("obstacle_layer").contains("track_unknown_space") ? common_cost_param.at("obstacle_layer").at("track_unknown_space").get<bool>() : true;
        always_send_full_costmap = common_cost_param.at("obstacle_layer").contains("always_send_full_costmap") ? common_cost_param.at("obstacle_layer").at("always_send_full_costmap").get<bool>() : false;
        std::cout << name << "=》load param: global_frame:" << global_frame_ << ",robot_base_frame:" << robot_base_frame_ << std::endl;

        // 作为规划器使用的主地图，并通过它管理各层地图。
        layered_costmap_ = new LayeredCostmap(global_frame_, rolling_window, track_unknown_space);

        // if (private_nh.hasParam("plugins"))
        if (custom_cost_param.contains("plugins"))
        {
            // XmlRpc::XmlRpcValue my_list;
            // 在参数服务器上获取plugins的参数，这里的插件即为各层子地图，每层子地图使用Pluginlib（ROS插件机制）来实例化，各个层可以被独立编译
            // private_nh.getParam("plugins", my_list);
            auto my_list = custom_cost_param.at("plugins").get<std::vector<std::string>>();
            ROS_INFO_STREAM("plugins size:" << my_list.size());
            for (int32_t i = 0; i < my_list.size(); ++i)
            {
                // 从my_list里获取地图名称和种类
                std::string pname = static_cast<std::string>(my_list[i]);
                ROS_INFO("%s: Using plugin \"%s\"", name_.c_str(), pname.c_str());

                boost::shared_ptr<Layer> plugin = nullptr;
                if (pname == "static_layer")
                {
                    plugin = boost::make_shared<costmap_2d::StaticLayer>(common_cost_param.at(pname));
                }
                else if (pname == "obstacle_layer")
                {
                    plugin = boost::make_shared<costmap_2d::ObstacleLayer>(common_cost_param.at(pname));
                }
                else if (pname == "inflation_layer")
                {
                    plugin = boost::make_shared<costmap_2d::InflationLayer>(common_cost_param.at(pname));
                }
                layered_costmap_->addPlugin(plugin);
                plugin->initialize(layered_costmap_, name + "/" + pname, &tf_);
            }
        }

        // subscribe to the footprint topic
        std::string topic_param, topic;
        // if (!private_nh.searchParam("footprint_topic", topic_param))
        // {
        //     topic_param = "footprint_topic";
        // }

        // private_nh.param(topic_param, topic, std::string("footprint"));
        // footprint_sub_ = private_nh.subscribe(topic, 1, &Costmap2DROS::setUnpaddedRobotFootprintPolygon, this);

        if (!private_nh.searchParam("published_footprint_topic", topic_param))
        {
            topic_param = "published_footprint";
        }

        // 发布的是根据机器人当前位置计算出来的实时footprint位置
        private_nh.param(topic_param, topic, std::string("footprint")); // TODO: revert to oriented_footprint in N-turtle
        footprint_pub_ = private_nh.advertise<geometry_msgs::PolygonStamped>(topic, 1);

        // setUnpaddedRobotFootprint(makeFootprintFromParams(private_nh));

        // 地图发布器
        publisher_ = new Costmap2DPublisher(&private_nh, layered_costmap_->getCostmap(), global_frame_, "costmap",
                                            always_send_full_costmap);

        // create a thread to handle updating the map
        stop_updates_ = false;
        initialized_ = true;
        stopped_ = false;

        // Create a time r to check if the robot is moving
        robot_stopped_ = false;
        // 回调函数movementCB通过比较前后两个pose的差来判断机器人是否移动
        // timer_ = private_nh.createTimer(ros::Duration(.1), &Costmap2DROS::movementCB, this);
        move_check_thread_ = new boost::thread(boost::bind(&Costmap2DROS::movementCB, this, 0.1));

        robot_pose_sub_ = private_nh.subscribe("/pose", 1, &Costmap2DROS::poseStampedCallback, this);

        reconfigureCB(custom_cost_param, common_cost_param);
        // 开启动态参数配置。加载.cfg配置参数
        // dsrv_ = new dynamic_reconfigure::Server<Costmap2DConfig>(ros::NodeHandle("~/" + name));
        // // 除了给相应的类成员进行赋值，主要是会开启一个map更新的线程
        // dynamic_reconfigure::Server<Costmap2DConfig>::CallbackType cb = boost::bind(&Costmap2DROS::reconfigureCB, this, _1,
        //                                                                             _2);
        // dsrv_->setCallback(cb);
    }

    // void Costmap2DROS::setUnpaddedRobotFootprintPolygon(const geometry_msgs::Polygon &footprint)
    // {
    //     ROS_WARN("activate setUnpaddedRobotFootprintPolygon\n");
    //     setUnpaddedRobotFootprint(toPointVector(footprint));
    // }

    Costmap2DROS::~Costmap2DROS()
    {
        timer_.stop();

        map_update_thread_shutdown_ = true;
        if (map_update_thread_ != NULL)
        {
            map_update_thread_->join();
            delete map_update_thread_;
        }
        if (move_check_thread_ != NULL)
        {
            move_check_thread_->join();
            delete move_check_thread_;
        }

        if (publisher_ != NULL)
            delete publisher_;

        delete layered_costmap_;
    }
    void Costmap2DROS::reconfigureCB(nlohmann::json &c_param, nlohmann::json &o_param)
    {
        transform_tolerance_ = c_param.contains("transform_tolerance") ? c_param.at("transform_tolerance").get<double>() : 0.3;
        double map_update_frequency = c_param.contains("update_frequency") ? c_param.at("update_frequency").get<double>() : 5;
        double map_publish_frequency = c_param.contains("publish_frequency") ? c_param.at("publish_frequency").get<double>() : 0;
        double map_width_meters = c_param.contains("width") ? c_param.at("width").get<double>() : 10;
        double map_height_meters = c_param.contains("height") ? c_param.at("height").get<double>() : 10;
        double resolution = c_param.contains("resolution") ? c_param.at("resolution").get<double>() : 0.05;
        double origin_x = c_param.contains("origin_x") ? c_param.at("origin_x").get<double>() : 0;
        double origin_y = c_param.contains("origin_y") ? c_param.at("origin_y").get<double>() : 0;
        double footprint_padding = o_param.contains("footprint_padding") ? o_param.at("footprint_padding").get<double>() : 0.01;
        auto footprint_temp = o_param.contains("footprint") ? o_param.at("footprint").get<std::vector<std::vector<double>>>() : std::vector<std::vector<double>>();
        std::vector<costmap_2d::Point> footprint;
        for (auto &pt : footprint_temp)
        {
            footprint.emplace_back(costmap_2d::Point(pt[0], pt[1]));
        }

        if (map_update_thread_ != NULL)
        {
            map_update_thread_shutdown_ = true;
            map_update_thread_->join();
            delete map_update_thread_;
            map_update_thread_ = NULL;
        }
        map_update_thread_shutdown_ = false;

        map_publish_frequency_ = map_publish_frequency;
        // if (map_publish_frequency > 0)
        //     publish_cycle = ros::Duration(1 / map_publish_frequency);
        // else
        //     publish_cycle = ros::Duration(-1);

        if (!layered_costmap_->isSizeLocked())
        {
            layered_costmap_->resizeMap((unsigned int)(map_width_meters / resolution),
                                        (unsigned int)(map_height_meters / resolution), resolution, origin_x, origin_y);
        }

        // If the padding has changed, call setUnpaddedRobotFootprint() to
        // re-apply the padding.
        if (footprint_padding_ != footprint_padding)
        {
            footprint_padding_ = footprint_padding;
            setUnpaddedRobotFootprint(unpadded_footprint_);
        }

        readFootprintFromConfig(footprint);

        // only construct the thread if the frequency is positive
        if (map_update_frequency > 0.0)
            map_update_thread_ = new boost::thread(boost::bind(&Costmap2DROS::mapUpdateLoop, this, map_update_frequency));
    }
    void Costmap2DROS::readFootprintFromConfig(const std::vector<costmap_2d::Point> &new_footprint)
    {
        if (old_footprint_.size() == new_footprint.size() ||
            std::equal(old_footprint_.begin(), old_footprint_.end(), new_footprint.begin(), new_footprint.end(),
                       [](const costmap_2d::Point &v1, const costmap_2d::Point &v2)
                       {
                           return v1 == v2;
                       }))
            return;

        old_footprint_ = new_footprint;
        if (!new_footprint.empty())
        {
            // std::vector<geometry_msgs::Point> geo_footprint;

            // geo_footprint.reserve(new_footprint.size());
            // for (unsigned int i = 0; i < new_footprint.size(); i++)
            // {
            //     if (new_footprint[i].size() == 2)
            //     {
            //         geometry_msgs::Point point;
            //         point.x = new_footprint[i][0];
            //         point.y = new_footprint[i][1];
            //         point.z = 0;
            //         geo_footprint.push_back(point);
            //     }
            //     else
            //     {
            //         ROS_ERROR("Points in the footprint specification must be pairs of numbers.  Found a point with %d numbers.",
            //                   int(new_footprint[i].size()));
            //     }
            // }

            std::vector<costmap_2d::Point> geo_footprint;
            // geo_footprint.reserve(new_footprint.size());
            for (auto &pt : new_footprint)
            {
                geo_footprint.push_back(pt);
            }

            setUnpaddedRobotFootprint(geo_footprint);
        }
    }

    // void Costmap2DROS::setUnpaddedRobotFootprint(const std::vector<geometry_msgs::Point> &points)
    // {
    //     unpadded_footprint_ = points;
    //     padded_footprint_ = points;
    //     padFootprint(padded_footprint_, footprint_padding_);

    //     layered_costmap_->setFootprint(padded_footprint_);
    // }
    void Costmap2DROS::setUnpaddedRobotFootprint(const std::vector<costmap_2d::Point> &points)
    {
        unpadded_footprint_ = points;
        padded_footprint_ = points;
        padFootprint(padded_footprint_, footprint_padding_);

        layered_costmap_->setFootprint(padded_footprint_);
    }
    void Costmap2DROS::movementCB(double duration)
    {

        while (true)
        {
            auto start_time = std::chrono::steady_clock::now();

            Eigen::Isometry2d new_pose;

            if (!getRobotPose(new_pose))
            {
                ROS_WARN_THROTTLE(1.0, "Could not get robot pose, cancelling reconfiguration");
                robot_stopped_ = false;
            }
            // make sure that the robot is not moving
            else
            {
                old_pose_ = new_pose;

                robot_stopped_ = ((old_pose_.translation() - new_pose.translation()).norm() < 1e-3) &&
                                 (fabs(Eigen::Rotation2Dd(old_pose_.linear()).smallestAngle() - Eigen::Rotation2Dd(new_pose.linear()).smallestAngle()) < 1e-3);
            }

            auto end_time = std::chrono::steady_clock::now();                                                     // 记录结束时间
            auto elapsed_time = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time); // 计算执行时间

            if (elapsed_time < std::chrono::duration<double>(duration))
            {
                std::this_thread::sleep_for(std::chrono::duration<double>(duration) - elapsed_time); // 等待剩余时间
            }
            else
            {
                // 执行时间超过定时器时间间隔，输出警告信息
                std::cout << "Timer callback took longer than " << duration << " seconds" << std::endl;
            }
        }

    }

    void Costmap2DROS::mapUpdateLoop(double frequency)
    {
        ros::NodeHandle nh;
        // ros::Rate r(frequency);
        std::chrono::duration<double> loop_cycle_time(1.0 / frequency);                 // 计算周期时间
        std::chrono::duration<double> publish_cycle_time(1.0 / map_publish_frequency_); // 计算周期时间

        auto before_time = std::chrono::steady_clock::now();
        while (nh.ok() && !map_update_thread_shutdown_)
        {
            auto start_time = std::chrono::steady_clock::now();

#ifdef HAVE_SYS_TIME_H
            struct timeval start, end;
            double start_t, end_t, t_diff;
            gettimeofday(&start, NULL);
#endif

            ROS_WARN("had in mapUpdateLoop");
            updateMap();

#ifdef HAVE_SYS_TIME_H
            gettimeofday(&end, NULL);
            start_t = start.tv_sec + double(start.tv_usec) / 1e6;
            end_t = end.tv_sec + double(end.tv_usec) / 1e6;
            t_diff = end_t - start_t;
            ROS_DEBUG("Map update time: %.9f", t_diff);
#endif

            // 更新地图边界及发布
            if (map_publish_frequency_ > 0 && layered_costmap_->isInitialized())
            {
                unsigned int x0, y0, xn, yn;
                layered_costmap_->getBounds(&x0, &xn, &y0, &yn);

                publisher_->updateBounds(x0, xn, y0, yn);

                // ros::Time now = ros::Time::now();
                auto cur_time = std::chrono::steady_clock::now();
                if (before_time + publish_cycle_time < start_time)
                {
                    publisher_->publishCostmap();
                    before_time = cur_time;
                }
                // if (last_publish_ + publish_cycle < now)
                // {
                //     publisher_->publishCostmap();
                //     last_publish_ = now;
                // }
            }

            auto end_time = std::chrono::steady_clock::now(); // 记录结束时间
            auto elapsed_time = end_time - start_time;        // 计算执行时间
            if (elapsed_time < loop_cycle_time)
            {
                std::this_thread::sleep_for(loop_cycle_time - elapsed_time); // 等待剩余时间
            }
            else
            {
                // 执行时间超过周期时间，输出警告信息
                double actual_frequency = 1.0 / elapsed_time.count();
                std::cout << "Map update loop missed its desired rate of " << frequency << "Hz... the loop actually took "
                          << elapsed_time.count() << " seconds (actual frequency: " << actual_frequency << "Hz)" << std::endl;
            }
            // r.sleep();
            // // make sure to sleep for the remainder of our cycle time
            // if (r.cycleTime() > ros::Duration(1 / frequency))
            //     ROS_WARN("Map update loop missed its desired rate of %.4fHz... the loop actually took %.4f seconds", frequency,
            //              r.cycleTime().toSec());
        }
    }

    // 这里的更新，主要是获取机器人当前位置来更新地图
    void Costmap2DROS::updateMap()
    {
        if (!stop_updates_)
        {
            // get global pose
            // geometry_msgs::PoseStamped pose;
            Eigen::Isometry2d pose;

            if (getRobotPose(pose))
            {
                //   double x = pose.pose.position.x,
                //  y = pose.pose.position.y,
                //  yaw = tf2::getYaw(pose.pose.orientation);
                double x = pose.translation().x(),
                       y = pose.translation().y(),
                       yaw = Eigen::Rotation2Dd(pose.linear()).smallestAngle();

                ROS_WARN("updateMap pose: %f,%f,%f", x, y, yaw);

                layered_costmap_->updateMap(x, y, yaw);

                // 更新机器人实时足迹

                std::vector<costmap_2d::Point> footprint_vec;
                transformFootprint(x, y, yaw, padded_footprint_, footprint_vec);
                publisher_->setFootprint(footprint_vec);
                // publisher_->setRobotPose(costmap_2d::Point(x, y));

                geometry_msgs::PolygonStamped footprint;
                footprint.header.frame_id = global_frame_;
                footprint.header.stamp = ros::Time::now();
                for (auto &pt : footprint_vec)
                {
                    geometry_msgs::Point32 p;
                    p.x = pt.x();
                    p.y = pt.y();
                    std::cout << " footprint.polygon:" << p.x << "," << p.y << std::endl;
                    footprint.polygon.points.push_back(p);
                }
                footprint_pub_.publish(footprint);

                initialized_ = true;
            }
        }
    }

    // 激活各层地图， 被MoveBase中调用
    void Costmap2DROS::start()
    {
        ROS_INFO("Costmap2DROS call start!!");
        std::vector<boost::shared_ptr<Layer>> *plugins = layered_costmap_->getPlugins();
        // check if we're stopped or just paused
        if (stopped_)
        {
            // if we're stopped we need to re-subscribe to topics
            for (vector<boost::shared_ptr<Layer>>::iterator plugin = plugins->begin(); plugin != plugins->end();
                 ++plugin)
            {
                (*plugin)->activate();
            }
            stopped_ = false;
        }
        stop_updates_ = false;

        // block until the costmap is re-initialized.. meaning one update cycle has run
        // note: this does not hold, if the user has disabled map-updates allgother
        //   ros::Rate r(100.0);
        //   while (ros::ok() && !initialized_ && map_update_thread_)
        //   {
        //     ROS_WARN("costmap_2d_ros start block loop");
        //     if(initialized_)
        //     {
        //         ROS_WARN("initialized_ IS TRUE");
        //     }else
        //     {
        //         ROS_WARN("initialized_ IS FALSE");

        //     }
        //     r.sleep();

        //   }
    }

    void Costmap2DROS::stop()
    {
        stop_updates_ = true;
        std::vector<boost::shared_ptr<Layer>> *plugins = layered_costmap_->getPlugins();
        // unsubscribe from topics
        for (vector<boost::shared_ptr<Layer>>::iterator plugin = plugins->begin(); plugin != plugins->end();
             ++plugin)
        {
            (*plugin)->deactivate();
        }
        initialized_ = false;
        stopped_ = true;
    }

    void Costmap2DROS::pause()
    {
        stop_updates_ = true;
        initialized_ = false;
    }

    void Costmap2DROS::resume()
    {
        stop_updates_ = false;

        // block until the costmap is re-initialized.. meaning one update cycle has run
        ros::Rate r(100.0);
        while (!initialized_)
            r.sleep();
    }

    void Costmap2DROS::resetLayers()
    {
        Costmap2D *top = layered_costmap_->getCostmap();
        top->resetMap(0, 0, top->getSizeInCellsX(), top->getSizeInCellsY());
        std::vector<boost::shared_ptr<Layer>> *plugins = layered_costmap_->getPlugins();
        for (vector<boost::shared_ptr<Layer>>::iterator plugin = plugins->begin(); plugin != plugins->end();
             ++plugin)
        {
            (*plugin)->reset();
        }
    }
    void Costmap2DROS::poseStampedCallback(const geometry_msgs::PoseStamped &odom_pose)
    {
        ROS_WARN("[AIDEN] poseStampedCallback");
        // TODO: 增加mutex保护

        if (odom_pose.header.frame_id == "pose_foot_to_map")
        {
            std::unique_lock<std::mutex> lock(mtx_pose);

            double yaw = tf2::getYaw(odom_pose.pose.orientation); // 解Quaternions获得yaw旋转
            auto robot_pose = toIsometry2d(odom_pose.pose.position.x, odom_pose.pose.position.y, yaw);
            ROS_WARN_STREAM("##get robot_pose:" << robot_pose.translation().x() << "," << robot_pose.translation().y() << ","
                                                << Eigen::Rotation2Dd(robot_pose.linear()).smallestAngle());

            robot_pose_deque_.emplace_back(robot_pose);
            if (robot_pose_deque_.size() > 1)
            {
                robot_pose_deque_.pop_front();
            }
        }
    }
    // need:  /base_footprint convert to /map
    // bool Costmap2DROS::getRobotPose(Eigen::Isometry2d &global_pose) const
    // {
    //             std::unique_lock<std::mutex> lock(mtx_pose);

    //         if (robot_pose_deque_.empty())
    //         {
    //             ROS_INFO_ONCE("robot_pose_deque_ is empty");
    //             return false;
    //         }
    //         global_pose = robot_pose_deque_.back();
    //             ROS_WARN_STREAM("getRobotPose:" << global_pose.translation().x() << "," << global_pose.translation().y() << ","
    //                                         << Eigen::Rotation2Dd(global_pose.linear()).smallestAngle());

    //         return true;

    // }
    // bool Costmap2DROS::getRobotPose(geometry_msgs::PoseStamped& global_pose) const
    bool Costmap2DROS::getRobotPose(Eigen::Isometry2d &m_global_pose) const
    {
        geometry_msgs::PoseStamped global_pose;
        tf2::toMsg(tf2::Transform::getIdentity(), global_pose.pose);
        geometry_msgs::PoseStamped robot_pose;
        tf2::toMsg(tf2::Transform::getIdentity(), robot_pose.pose);
        robot_pose.header.frame_id = robot_base_frame_;
        robot_pose.header.stamp = ros::Time();
        ros::Time current_time = ros::Time::now(); // save time for checking tf delay later

        // get the global pose of the robot
        try
        {
            // use current time if possible (makes sure it's not in the future)
            if (tf_.canTransform(global_frame_, robot_base_frame_, current_time))
            {
                geometry_msgs::TransformStamped transform = tf_.lookupTransform(global_frame_, robot_base_frame_, current_time);
                tf2::doTransform(robot_pose, global_pose, transform);
            }
            // use the latest otherwise
            else
            {
                tf_.transform(robot_pose, global_pose, global_frame_);
            }
        }
        catch (tf2::LookupException &ex)
        {
            ROS_ERROR_THROTTLE(1.0, "No Transform available Error looking up robot pose: %s\n", ex.what());
            return false;
        }
        catch (tf2::ConnectivityException &ex)
        {
            ROS_ERROR_THROTTLE(1.0, "Connectivity Error looking up robot pose: %s\n", ex.what());
            return false;
        }
        catch (tf2::ExtrapolationException &ex)
        {
            ROS_ERROR_THROTTLE(1.0, "Extrapolation Error looking up robot pose: %s\n", ex.what());
            return false;
        }
        // check global_pose timeout
        if (current_time.toSec() - global_pose.header.stamp.toSec() > transform_tolerance_)
        {
            ROS_WARN_THROTTLE(1.0,
                              "Costmap2DROS transform timeout. Current time: %.4f, global_pose stamp: %.4f, tolerance: %.4f",
                              current_time.toSec(), global_pose.header.stamp.toSec(), transform_tolerance_);
            return false;
        }

        m_global_pose = toIsometry2d(global_pose.pose.position.x, global_pose.pose.position.y, tf2::getYaw(global_pose.pose.orientation));
        return true;
    }

    void Costmap2DROS::getOrientedFootprint(std::vector<costmap_2d::Point> &oriented_footprint) const
    {
        //   geometry_msgs::PoseStamped global_pose;
        Eigen::Isometry2d global_pose;

        if (!getRobotPose(global_pose))
            return;

        double yaw = Eigen::Rotation2Dd(global_pose.linear()).smallestAngle();
        transformFootprint(global_pose.translation().x(), global_pose.translation().y(), yaw,
                           padded_footprint_, oriented_footprint);
    }

} // namespace costmap_2d

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <sensor_msgs/LaserScan.h>
#include <tf2_msgs/TFMessage.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>

#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include <map>

void publish_pose(ros::Publisher &pose_pub, const std::string &frame_id, const tf::StampedTransform &transformStamped)
{
    geometry_msgs::PoseStamped poseMsg;

    poseMsg.header.stamp = ros::Time(transformStamped.stamp_);
    // poseMsg.header.frame_id = "map"; // optional, this could be whichever you want
    poseMsg.header.frame_id = frame_id; // optional, this could be whichever you want
    poseMsg.pose.position.x = transformStamped.getOrigin().getX();
    poseMsg.pose.position.y = transformStamped.getOrigin().getY();
    poseMsg.pose.position.z = transformStamped.getOrigin().getZ();
    poseMsg.pose.orientation.x = transformStamped.getRotation().getX();
    poseMsg.pose.orientation.y = transformStamped.getRotation().getY();
    poseMsg.pose.orientation.z = transformStamped.getRotation().getZ();
    poseMsg.pose.orientation.w = transformStamped.getRotation().getW();
    // ROS_INFO("x: %f, y: %f, z: %f", poseMsg.pose.position.x, poseMsg.pose.position.y, poseMsg.pose.position.z);
    // Now you could publish this PoseStamped here
    pose_pub.publish(poseMsg);
}
void publish_tf(tf::TransformBroadcaster &br, const geometry_msgs::TransformStamped &transformStamped)
{
    br.sendTransform(transformStamped);
}
void publish_laser(ros::Publisher &laser_pub, const sensor_msgs::LaserScan &scan)
{
    ROS_INFO("lidar_frame_id: %s", scan.header.frame_id.c_str());
    laser_pub.publish(scan);
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "tf_to_posestamped_node");
    // ros::NodeHandle node("~"); // 所有发布的话题都会是/tf_to_posestamped_node/<topic>
    ros::NodeHandle node;

    rosbag::Bag read_bag;
    read_bag.open("/home/aiden/PROJECT/Catkin_Sim_Robot/src/costmap_2d_kubot/datas/test/simple_driving_test_indexed.bag", rosbag::bagmode::Read);

    // rosbag::Bag write_bag;
    // write_bag.open("/home/aiden/PROJECT/Catkin_Sim_Robot/devel/share/costmap_2d/test/simple_driving_test_indexed_new.bag", rosbag::bagmode::Write);

    std::vector<std::string> topics;
    topics.push_back(std::string("/base_scan"));
    topics.push_back(std::string("/tf"));

    rosbag::View view(read_bag, rosbag::TopicQuery(topics));

    ros::Publisher pose_pub = node.advertise<geometry_msgs::PoseStamped>("pose", 10);
    tf::TransformBroadcaster br;
    ros::Publisher laser_pub_raw = node.advertise<sensor_msgs::LaserScan>("base_scan", 1);

    int index = 0;
    // std::vector<geometry_msgs::TransformStamped> bound_tfs;
    std::multimap<double, std::pair<geometry_msgs::TransformStamped, sensor_msgs::LaserScan>> bound_message;

    for (rosbag::View::iterator it = view.begin(); it != view.end() && ros::ok(); ++it)
    {
        const rosbag::MessageInstance &m = *it;

        tf2_msgs::TFMessage::ConstPtr i = m.instantiate<tf2_msgs::TFMessage>();
        auto ls = m.instantiate<sensor_msgs::LaserScan>();

        if (ls != nullptr)
        {
            double cur_t = ls->header.stamp.toSec() + ls->header.stamp.toNSec() * 1e-9;

            ROS_INFO_STREAM("##lidar_stamp:" << cur_t);
            // publish_laser(laser_pub_raw, *ls);
            geometry_msgs::TransformStamped null_tf;
            null_tf.header.frame_id = "null";
            bound_message.insert({cur_t, {null_tf, *ls}});
        }

        if (i != nullptr)
        {

            for (auto &transformStamped : i->transforms)
            {
                double cur_t = transformStamped.header.stamp.toSec() + transformStamped.header.stamp.toNSec() * 1e-9;
                ROS_INFO_STREAM("##tf_stamp:" << cur_t);

                // ROS_INFO_STREAM("##stamp:" << transformStamped.header.stamp.toSec() << ",nsec:" << transformStamped.header.stamp.toNSec()
                //                            << ", frame_id: " << transformStamped.header.frame_id.c_str()
                //                            << ", child_frame_id: " << transformStamped.child_frame_id.c_str());

                sensor_msgs::LaserScan null_lidar;
                null_lidar.header.frame_id = "null";
                bound_message.insert({cur_t, {transformStamped, null_lidar}});
            }
        }
    }
    tf::StampedTransform tf_odom_to_map, tf_foot_to_odom, tf_link_to_foot, tf_laser_to_link;

    int loop = 0;
    double before_t;
    bool first_flag = true;
    ros::Rate rate(10);

    for (auto &temp_msg : bound_message)
    {
        if (!ros::ok())
            break;

        if (temp_msg.second.second.header.frame_id == "null")
        {
            auto temp_tf = temp_msg.second.first;
            auto cur_t = temp_tf.header.stamp.toSec() + temp_tf.header.stamp.toNSec() * 1e-9;

            ROS_INFO_STREAM("#tf_stamp:" << cur_t
                                         << ", frame_id: " << temp_tf.header.frame_id.c_str()
                                         << ", child_frame_id: " << temp_tf.child_frame_id.c_str());

            if (before_t == cur_t || first_flag)
            {
                if (temp_tf.header.frame_id == "/map")
                {
                    tf::transformStampedMsgToTF(temp_tf, tf_odom_to_map);
                }
                else if (temp_tf.header.frame_id == "/odom")
                {
                    tf::transformStampedMsgToTF(temp_tf, tf_foot_to_odom);
                }
                else if (temp_tf.header.frame_id == "/base_footprint")
                {
                    tf::transformStampedMsgToTF(temp_tf, tf_link_to_foot);
                }
                else if (temp_tf.header.frame_id == "/base_link")
                {
                    tf::transformStampedMsgToTF(temp_tf, tf_laser_to_link);
                }
                else
                {
                    ROS_INFO("error frame_id: %s", temp_tf.header.frame_id.c_str());
                }
                publish_tf(br, temp_tf);

                loop++;
                first_flag = false;
                ROS_INFO("loop: %d", loop);
            }
            before_t = cur_t;

            if (loop == 4)
            {
                tf::StampedTransform final_laser_map, final_foot_map, final_laser_link;
                /** final_laser_map = tf_odom_to_map*tf_foot_to_odom*tf_link_to_foot*tf_laser_to_link
                final_foot_map = tf_odom_to_map*tf_foot_to_odom **/

                final_foot_map.mult(tf_odom_to_map, tf_foot_to_odom);
                final_laser_link.mult(tf_link_to_foot, tf_laser_to_link);
                final_laser_map.mult(final_foot_map, final_laser_link);

                ROS_WARN("pose_foot_to_map: x: %f, y: %f, z: %f", final_foot_map.getOrigin().getX(), final_foot_map.getOrigin().getY(), final_foot_map.getOrigin().getZ());
                publish_pose(pose_pub, "pose_foot_to_map", final_foot_map);
                publish_pose(pose_pub, "pose_laser_to_map", final_laser_map);

                loop = 0;
                first_flag = true;
            }
        }
        else
        {
            auto temp_lidar = temp_msg.second.second;

            double cur_t = temp_lidar.header.stamp.toSec() + temp_lidar.header.stamp.toNSec() * 1e-9;

            ROS_INFO_STREAM("##lidar_stamp:" << cur_t);

            publish_laser(laser_pub_raw, temp_lidar);
        }

        ros::spinOnce();
        // rate.sleep();
        // sleep(1);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

#if 0
        sensor_msgs::LaserScan::ConstPtr scan = m.instantiate<sensor_msgs::LaserScan>();
        std::cout << "index:" << ++index << std::endl;
        if (scan != NULL)
        {
            sensor_msgs::LaserScan new_scan = *scan;
            auto t_time = new_scan.header.stamp.toNSec();
            std::cout << "t_time:" << t_time << std::endl;
            new_scan.header.stamp.sec = new_scan.header.stamp.sec * 10000;
            new_scan.header.stamp.nsec = new_scan.header.stamp.nsec * 10000;
            write_bag.write(m.getTopic(), m.getTime(), new_scan);
        }
#endif

    read_bag.close();
    // write_bag.close();
    return 0;
}
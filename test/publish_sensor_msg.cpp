#include <fstream>
#include <ostream>
#include <sstream>
#include <iostream>
#include <vector>
#include <array>
#include <tuple>
#include <thread>
#include <chrono>

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf2_msgs/TFMessage.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>

struct LidarData
{
    uint32_t lidar_index;
    uint32_t ranges_len;
    uint32_t intensities_len;
    std::vector<float> ranges;
    std::vector<float> intensities;
};
using LidarPair = std::pair<uint64_t, LidarData>;
using PosePair = std::pair<uint64_t, std::array<double, 3>>;
LidarPair readLidarData(std::ifstream &infile, const std::string &lidar_name)
{
    LidarData lidarData;
    uint64_t time_stamp;

    infile.read(reinterpret_cast<char *>(&time_stamp), sizeof(uint64_t));

    size_t range_size;
    infile.read(reinterpret_cast<char *>(&range_size), sizeof(size_t));
    std::cout << " range_size:" << range_size << std::endl;

    infile.read(reinterpret_cast<char *>(&lidarData.lidar_index), sizeof(uint32_t));
    infile.read(reinterpret_cast<char *>(&lidarData.ranges_len), sizeof(uint32_t));
    infile.read(reinterpret_cast<char *>(&lidarData.intensities_len), sizeof(uint32_t));

    // size_t num = 1600;
    size_t num = range_size;
    lidarData.ranges.resize(num);
    infile.read(reinterpret_cast<char *>(lidarData.ranges.data()), num * sizeof(float));

    lidarData.intensities.resize(num);
    infile.read(reinterpret_cast<char *>(lidarData.intensities.data()), num * sizeof(float));

    std::cout << "time_stamp:" << time_stamp << ",LidarData: " << lidar_name << " lidar_index:" << lidarData.lidar_index << " ranges_len:" << lidarData.ranges_len << " intensities_len:" << lidarData.intensities_len << std::endl;
    // std::cout << "lidar data:";
    // for(auto& data: lidarData.ranges)
    // {
    //     std::cout <<data << ",";
    // }
    //  std::cout << std::endl;

    return std::make_pair(time_stamp, lidarData);
}
PosePair readPose(std::ifstream &infile)
{
    std::array<double, 3> pose;
    uint64_t time_stamp;

    infile.read(reinterpret_cast<char *>(&time_stamp), sizeof(uint64_t));
    infile.read(reinterpret_cast<char *>(pose.data()), 3 * sizeof(double));

    std::cout << "time_stamp:" << time_stamp << ",pose: " << pose[0] << " " << pose[1] << " " << pose[2] << std::endl;
    return std::make_pair(time_stamp, pose);
}
void publish_laser(ros::Publisher &laser_pub, const LidarPair &lidar_data)
{
    std::cout << "lidar_data time:" << lidar_data.first << std::endl;
    sensor_msgs::LaserScan scan;
    ros::Time ros_time;
    ros_time.sec = lidar_data.first / 1000000000;
    ros_time.nsec = lidar_data.first % 1000000000;
    scan.header.stamp = ros_time;
    scan.header.frame_id = "base_laser";
    scan.angle_min = 1.04523;
    scan.angle_max = 5.23795;
    scan.angle_increment = 0.00392945;
    scan.time_increment = 4.16927e-05;
    scan.scan_time = 0.0666667;
    scan.range_min = 0.001;
    scan.range_max = 10.0;
    scan.ranges = lidar_data.second.ranges;
    scan.intensities = lidar_data.second.intensities;
    laser_pub.publish(scan);
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "sensor_publisher");
    ros::NodeHandle nh;

    std::string filename = "/home/aiden/PROJECT/Catkin_Sim_Robot/src/costmap_2d_kubot/datas/sensor_data_20231207_191109_668.data";
    std::ifstream inFile(filename, std::ios::binary);

    std::deque<LidarPair> lidar_data_sets;
    std::deque<PosePair> pose_data_sets;
    if (inFile.is_open())
    {
        char id;
        while (!inFile.eof())
        {
            inFile.read(reinterpret_cast<char *>(&id), sizeof(char));

            switch (id)
            {
            case 0x01:
            {
                lidar_data_sets.emplace_back(readLidarData(inFile, "base_laser"));
            }
            break;
            case 0x03:
            {
                readLidarData(inFile, "back_lidar");
            }
            break;
            case 0x05:
            {
                pose_data_sets.emplace_back(readPose(inFile));
            }
            break;
            default:
            {
                std::cout << "error get id: " << std::hex << id << std::endl;
            }
            break;
            }
        }
        inFile.close();
    }
    std::cout << "finish read data\n";
    std::cout << "lidar size:" << lidar_data_sets.size() << ",pose size:" << pose_data_sets.size()  <<std::endl;
    ros::Publisher laser_pub_raw = nh.advertise<sensor_msgs::LaserScan>("scan_raw", 1);

    ros::Rate r(10);
    int loop = 0;
    while (nh.ok() || (!lidar_data_sets.empty() || !pose_data_sets.empty()))
    // while (nh.ok())
    {

        std::cout << "first t" << lidar_data_sets.front().first << std::endl;
        publish_laser(laser_pub_raw, lidar_data_sets.front());
        lidar_data_sets.pop_front();
        std::cout << "loop:" << ++loop << std::endl;
        ros::spinOnce();
        // r.sleep();

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        
    }

    return 0;
}
#include <ros/ros.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/cost_values.h>
#include <tf2_ros/transform_listener.h>

#include "nlohmann/json.hpp"
#include <fstream>
#include <iostream>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "aiden_costmap_node");

    std::ifstream jfile;
    jfile.open("/home/aiden/PROJECT/Catkin_Sim_Robot/src/costmap_2d_kubot/params/aiden_costmap_example.json");
    if (!jfile.is_open())
    {
        ROS_ERROR("Failed to open json file");
        return -1;
    }
    nlohmann::json j_param;
    jfile >> j_param;

    tf2_ros::Buffer buffer(ros::Duration(10));
    tf2_ros::TransformListener tf(buffer);

    sleep(1); //等待map完成

    costmap_2d::Costmap2DROS *planner_costmap_ros_, *controller_costmap_ros_;

    ros::NodeHandle n;
    ros::NodeHandle private_nh("~");

    // # 全局规划器代价地图
    planner_costmap_ros_ = new costmap_2d::Costmap2DROS("global_costmap", j_param, buffer);
    planner_costmap_ros_->pause();

    // # 局部规划器代价地图
    controller_costmap_ros_ = new costmap_2d::Costmap2DROS("local_costmap", j_param, buffer);
    controller_costmap_ros_->pause();

    // #类函数 start() 会调用各层地图的 active() 函数，开始订阅传感器话题，对地图进行更新
    planner_costmap_ros_->start();
    controller_costmap_ros_->start();

    bool shutdown_costmaps_ = false;
    if (shutdown_costmaps_)
    {
        ROS_DEBUG_NAMED("move_base", "Stopping costmaps initially");
        // planner_costmap_ros_->stop();
        // controller_costmap_ros_->stop();
    }

    // calcute the cost of a point (1.5,2.5)
    // unsigned int mx, my;
    // double wx = 1.5, wy = 2.5;
    // planner_costmap_ros_->getCostmap()->worldToMap(wx, wy, mx, my);
    // unsigned char cost = planner_costmap_ros_->getCostmap()->getCost(mx, my);
    // ROS_INFO("cost: %d", cost);

    // calcuate the polygon area cost
    // auto footprint = planner_costmap_ros_->getRobotFootprint();
    // double footprint_cost = planner_costmap_ros_->getCostmap()->footprintCost(1.5, 2.5, 0.0, footprint);
    // ROS_INFO("footprint_cost: %f", footprint_cost);

    ros::spin();
    return 0;
}